#include <iostream>
#include <random>
#include <limits>


#include "Encoder.h"
#include "cpu_solver.h"
#include "GPU_SOLVER.cuh"
#include "OMP_SOLVER.h"

Encoder::Encoder(cv::Mat host, cv::Mat mark, int t, ENCODER_SOLVER es) {
    setThreads(t);
    setRGB(mark);
    setEncoderSolver(es);
    computeHostHalftone(host);
    computeMarkHalftones();
    computeHalftoneMasks(hostHalftone);
    computeTriplets();
    embedMark();
    convertEmbeddedMasksToHalftone();
    //std::cout << codedHalftone << std::endl;
}

void Encoder::setThreads(int t) {
    threads = t;
}

void Encoder::setEncoderSolver(ENCODER_SOLVER es) {
    encoderSolver = es;
}

Encoder::~Encoder() {
    //dtor
}

cv::Mat Encoder::getHostHalftone(void) {
    return 255 * hostHalftone;
}

cv::Mat Encoder::getMasked(void) {
    return 255 * codedHalftone;
}

void Encoder::convertEmbeddedMasksToHalftone(void) {
    uint cols = hR.cols;
    uint rows = hR.rows;
    codedHalftone = cv::Mat::zeros(BLOCK_SIZE * rows, BLOCK_SIZE * cols, CV_8UC1);
    uint k = 0;
    for (uint row = 0; row < rows; row++) {
        for (uint col = 0; col < cols; col++) {
            mask_t m = embeddedMasks[k++];
            cv::Mat block = getMaskAsBlock(m);
            cv::Rect roi = cv::Rect(col * BLOCK_SIZE, row * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
            //std::cout << row << "," << col << " -> " << roi << std::endl;
            block.copyTo(codedHalftone(roi));
        }
    }
}


void Encoder::setEmbeddedMasks(std::vector<mask_t> m) {
    embeddedMasks = m;
}


void Encoder::embedMark(void) {
    switch (encoderSolver) {
    case GPU: 
        setEmbeddedMasks(GPU_SOLVER::computeBestMatches(triplets, maskedHalftoneBlocks));
        break;
    case CPU:
        setEmbeddedMasks(CPU_SOLVER::computeBestMatches(triplets, maskedHalftoneBlocks));
        break;
    case OMP:
        setEmbeddedMasks(OMP_SOLVER::computeBestMatches(triplets, maskedHalftoneBlocks, threads));
        break;
        //TODO
    }

}

void Encoder::computeTriplets(void) {
    cv::Mat img = this->hR;
    for (int r = 0; r < img.rows; r++) {
        for (int c = 0; c < img.cols; c++) {
            bool rr = hR.at<uchar>(r, c) != 0;
            bool gg = hG.at<uchar>(r, c) != 0;
            bool bb = hB.at<uchar>(r, c) != 0;
            triplet_t m = {
                rr, gg, bb
            };
            triplets.push_back(m);
        }
    }
}

void Encoder::computeHostHalftone(cv::Mat host) {
    cv::Mat resized;
    cv::resize(host, resized, cv::Size(0, 0), BLOCK_SIZE, BLOCK_SIZE, cv::INTER_CUBIC);
    this->hostHalftone = Halftoning::floydSteinberg(resized);
}

void Encoder::computeMarkHalftones(void) {
    this->hB = Halftoning::floydSteinberg(this->B);
    this->hG = Halftoning::floydSteinberg(this->G);
    this->hR = Halftoning::floydSteinberg(this->R);
}

void Encoder::setRGB(cv::Mat mark) {
    vector<cv::Mat> rgb;
    split(mark, rgb);
    this->B = rgb[0];
    this->G = rgb[1];
    this->R = rgb[2];
}
