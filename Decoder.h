#ifndef DECODER_H
#define DECODER_H

#include <opencv2/opencv.hpp>

#include "constants.h"
#include "shared_types.h"
#include "Halftoning.h"
#include "AbstractCodec.h"

class Decoder : AbstractCodec
{
    public:
        Decoder(cv::Mat);
        virtual ~Decoder();
        cv::Mat getDecoded(void);
    protected:
        vector<cv::Mat> channelHalftones;
        cv::Mat decoded;
        cv::Mat coded;

        void computeTripletsFromMasks(void);
        void recoverHalftoneChannels(void);
        void recoverColorChannels(void);
    private:
        vector<triplet_t> triplets;
};

#endif // DECODER_H
