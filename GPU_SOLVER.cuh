#ifndef GPU_SOLVER_H
#define GPU_SOLVER_H

#include <nppdefs.h>
#include <thrust/transform.h>
#include <thrust/functional.h>
#include <thrust/execution_policy.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/extrema.h>
#include <thrust/device_malloc.h>
#include <thrust/device_free.h>
#include <device_launch_parameters.h>
#include <vector>
#include "shared_types.h"

namespace GPU_SOLVER {
    std::vector<mask_t> computeBestMatches(std::vector<triplet_t> triplets, std::vector<mask_t> masks);
};

#endif // GPU_SOLVER_H