#include "AbstractCodec.h"

AbstractCodec::AbstractCodec()
{
    //ctor
}

AbstractCodec::~AbstractCodec()
{
    //dtor
}

cv::Mat AbstractCodec::getMaskAsBlock(mask_t mask) {
    cv::Mat blocks = cv::Mat::zeros(BLOCK_SIZE, BLOCK_SIZE, CV_8UC1);
    blocks.at<uchar>(0,0) = mask.m0;
    blocks.at<uchar>(0,1) = mask.m1;
    blocks.at<uchar>(0,2) = mask.m2;
    blocks.at<uchar>(1,0) = mask.m3;
    blocks.at<uchar>(1,1) = mask.m4;
    blocks.at<uchar>(1,2) = mask.m5;
    blocks.at<uchar>(2,0) = mask.m6;
    blocks.at<uchar>(2,1) = mask.m7;
    blocks.at<uchar>(2,2) = mask.m8;
    return blocks;
}

// TODO: change the blocking transformation
mask_t AbstractCodec::getBlockAsMask(cv::Mat block) {
    bool m0 = block.at<uchar>(0, 0) != 0;
    bool m1 = block.at<uchar>(0, 1) != 0;
    bool m2 = block.at<uchar>(0, 2) != 0;
    bool m3 = block.at<uchar>(1, 0) != 0;
    bool m4 = block.at<uchar>(1, 1) != 0;
    bool m5 = block.at<uchar>(1, 2) != 0;
    bool m6 = block.at<uchar>(2, 0) != 0;
    bool m7 = block.at<uchar>(2, 1) != 0;
    bool m8 = block.at<uchar>(2, 2) != 0;
    mask_t m = {
        m0, m1, m2, m3, m4, m5, m6, m7, m8
    };
    return m;
}


void AbstractCodec::computeHalftoneMasks(cv::Mat halftone) {
    cv::Mat img = halftone;
    for (int r = 0; r < img.rows; r += BLOCK_SIZE) {
        for (int c = 0; c < img.cols; c += BLOCK_SIZE) {
            cv::Range verticalRange = cv::Range(r, min(r + BLOCK_SIZE, img.rows));
            cv::Range horizontalRange = cv::Range(c, min(c + BLOCK_SIZE, img.cols));
            cv::Mat tile = img(verticalRange, horizontalRange);
            mask_t mask = getBlockAsMask(tile);
            maskedHalftoneBlocks.push_back(mask);
        }
    }
}
