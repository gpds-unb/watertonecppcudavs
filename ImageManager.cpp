#include "ImageManager.h"

ImageManager::ImageManager(cv::Mat m) {
  mat = m;
  rows = m.rows;
  cols = m.cols;
}

vector<bgr_pixel> ImageManager::getBgrVector() {
    vector<bgr_pixel> rawVector;
    for(int i=0; i < mat.rows; i++) {
        for(int j=0; j < mat.cols; j++) {
            bgr_pixel pixel = {
                mat.at<cv::Vec3b>(i, j)[0],
                mat.at<cv::Vec3b>(i, j)[1],
                mat.at<cv::Vec3b>(i, j)[2],
            };
            rawVector.push_back(pixel);
        }
    }
    return rawVector;
}
