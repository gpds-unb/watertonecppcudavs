#ifndef GPU_MASKMANAGER_H
#define GPU_MASKMANAGER_H

#include <thrust/device_vector.h>
#include "cpu_MaskManager.h"

namespace gpu_maskmanager {
    
    thrust::device_vector<mask_t> mapedMaks0;
    thrust::device_vector<mask_t> mapedMaks1;
    thrust::device_vector<mask_t> mapedMaks2;
    thrust::device_vector<mask_t> mapedMaks3;
    thrust::device_vector<mask_t> mapedMaks4;
    thrust::device_vector<mask_t> mapedMaks5;
    thrust::device_vector<mask_t> mapedMaks6;
    thrust::device_vector<mask_t> mapedMaks7;

    __host__ __device__ unsigned int tripletAsIndex(triplet_t t) {
        return t.t0 << 2 | t.t1 << 1 | t.t2;
    }

    __host__ __device__ unsigned int maskAsIndex(mask_t m) {
        return m.m0 << 8 | m.m1 << 7 | m.m2 << 6 | m.m3 << 5 | m.m4 << 4 | m.m5 << 3 | m.m6 << 2 | m.m7 << 1 | m.m8;
    }

    __host__ thrust::device_vector<mask_t> getMasks(triplet_t triplet) {
        uint idx = tripletAsIndex(triplet);
        switch (idx) {
        case 0:
            return mapedMaks0;
        case 1:
            return mapedMaks1;
        case 2:
            return mapedMaks2;
        case 3:
            return mapedMaks3;
        case 4:
            return mapedMaks4;
        case 5:
            return mapedMaks5;
        case 6:
            return mapedMaks6;
        default:
            return mapedMaks7;
        }
    }



    void copyToHostDevice(void) {
        cpu_MaskManager mm;
        vector<triplet_t> triplets = mm.getTriplets();

        for (triplet_t t : triplets) {
            vector<mask_t> masks = mm.getMasks(t);
            thrust::device_vector<mask_t> device(masks.size());
            thrust::copy(masks.begin(), masks.end(), device.begin());
            unsigned int idx = tripletAsIndex(t);

            switch (idx) {
            case 0:
                mapedMaks0 = device;
            case 1:
                mapedMaks1 = device;
            case 2:
                mapedMaks2 = device;
            case 3:
                mapedMaks3 = device;
            case 4:
                mapedMaks4 = device;
            case 5:
                mapedMaks5 = device;
            case 6:
                mapedMaks6 = device;
            default:
                mapedMaks7 = device;
            }
        }
    }

    void init() {
        copyToHostDevice();
    }
};
#endif