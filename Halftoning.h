#ifndef HALFTONING_H
#define HALFTONING_H

#include <opencv2/opencv.hpp>

namespace Halftoning
{
    cv::Mat floydSteinberg(cv::Mat);
};

#endif // HALFTONING_H
