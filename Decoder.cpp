#include <ppl.h>
#include "Decoder.h"

#include "FBIH.h"

Decoder::Decoder(cv::Mat c) {
    coded = c;
    computeHalftoneMasks(c);
    computeTripletsFromMasks();
    recoverHalftoneChannels();
    recoverColorChannels();
}

Decoder::~Decoder() {
    //dtor
}

cv::Mat Decoder::getDecoded(void) {
    return decoded;
}

// TODO: Use invhlt here
void Decoder::recoverColorChannels(void) {
    vector<cv::Mat> in = channelHalftones;
    vector<cv::Mat> out(3);
    //concurrency::parallel_transform(in.begin(), in.end(), out.begin(), FBIH::inverseHalftoning);
    std::transform(in.begin(), in.end(), out.begin(), FBIH::inverseHalftoning);
    cv::merge(out, decoded);
}

void Decoder::recoverHalftoneChannels(void) {
    uint width = coded.cols / BLOCK_SIZE;
    uint height = coded.rows / BLOCK_SIZE;
    cv::Mat hR = cv::Mat::zeros(height, width, CV_8UC1);
    cv::Mat hG = cv::Mat::zeros(height, width, CV_8UC1);
    cv::Mat hB = cv::Mat::zeros(height, width, CV_8UC1);
    uint k = 0;
    for (uint i = 0; i < height; i++) {
        for (uint j = 0; j < width; j++) {
            triplet_t triplet = triplets[k++];
            hR.at<uchar>(i, j) = (uchar)triplet.t0;
            hG.at<uchar>(i, j) = (uchar)triplet.t1;
            hB.at<uchar>(i, j) = (uchar)triplet.t2;
        }
    }
    channelHalftones = { hB, hG, hR };
}

void Decoder::computeTripletsFromMasks(void) {
    for (mask_t mask : maskedHalftoneBlocks) {
        triplet_t triplet = mm.getTripletFromMask(mask);
        triplets.push_back(triplet);
    }
}
