#ifndef IMAGEMANAGER_H
#define IMAGEMANAGER_H

#include <vector>
#include <opencv2/opencv.hpp>

#include "shared_types.h"

using namespace std;

class ImageManager
{
  public:
    ImageManager(cv::Mat);
    vector<bgr_pixel> getBgrVector(void);
    unsigned int rows, cols;
  private:
    cv::Mat mat;
};

#endif // IMAGEMANAGER_H
