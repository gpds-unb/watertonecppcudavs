#include "OMP_SOLVER.h"
#include "cpu_MaskManager.h"
#include "binary_distances.cuh"
#include <omp.h>

namespace OMP_SOLVER {
    mask_t getMinimumMask(mask_t& mask, vector<mask_t>* possibleMasks) {
        int size = (*possibleMasks).size();
        int minIndex = 0;
        float minimalDistance = std::numeric_limits<float>::max();

        for (int v = 0; v < size; v++) {
            mask_t possibleMask = (*possibleMasks)[v];
            float distance = BinaryDistances::getDiceDissimilarity(mask, possibleMask);
            if (distance < minimalDistance) {
                minimalDistance = distance;
                minIndex = v;
            }
        }
        return (*possibleMasks)[minIndex];
    }

    mask_t getBestMatch(triplet_t& triplet, mask_t& mask, cpu_MaskManager& mm) {
        std::vector<mask_t> possibleMasks = mm.getMasks(triplet);
        return getMinimumMask(mask, &possibleMasks);
    }

    std::vector<mask_t> computeBestMatches(vector<triplet_t> triplets, vector<mask_t> masks, int threads=8) {
        std::vector<mask_t> stdChosenMask(masks.size());
        int size = triplets.size();

        omp_set_dynamic(0);
        omp_set_num_threads(threads);

#pragma omp parallel 
        {
            cpu_MaskManager mm;
#pragma omp for
            for (int v = 0; v < size; ++v) {
                stdChosenMask[v] = getBestMatch(triplets[v], masks[v], mm);
            }
        }
        return stdChosenMask;
    }
};