#ifndef cpu_MaskManager_H
#define cpu_MaskManager_H

#include <vector>
#include <map>

#include "shared_types.h"
#include "product.h"

using namespace std;

class cpu_MaskManager
{
    public:
        cpu_MaskManager();
        virtual ~cpu_MaskManager();
        vector<mask_t> getMasks(triplet_t);
        vector<mask_t> getMasks(void);
        vector<triplet_t> getTriplets(void);
        triplet_t getTripletFromMask(mask_t);
    protected:
    private:
        void create_masks(void);
        void create_triplets(void);
        void create_maps(void);

        vector<triplet_t> triplets;
        vector<mask_t> masks;
        map<triplet_t, vector<mask_t>> mapedMaks;
        map<mask_t, triplet_t> mapedTriplets;
};

#endif // cpu_MaskManager_H
