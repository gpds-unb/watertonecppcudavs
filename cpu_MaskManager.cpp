#include "cpu_MaskManager.h"

cpu_MaskManager::cpu_MaskManager() {
    create_masks();
    create_triplets();
    create_maps();
}

cpu_MaskManager::~cpu_MaskManager() {
    //dtor
}

triplet_t cpu_MaskManager::getTripletFromMask(mask_t m) {
    return mapedTriplets[m];
}

vector<mask_t> cpu_MaskManager::getMasks(triplet_t triplet) {
    return mapedMaks[triplet];
}

vector<mask_t> cpu_MaskManager::getMasks(void) {
    return masks;
}

vector<triplet_t> cpu_MaskManager::getTriplets(void) {
    return triplets;
}

void cpu_MaskManager::create_masks(void) {
    vector<bool> v {0, 1};
    for(auto&& t: iter::product(v, v, v, v, v, v, v, v, v)) {
        mask_t m = {
            get<0>(t), get<1>(t), get<2>(t), get<3>(t), get<4>(t), get<5>(t), get<6>(t), get<7>(t), get<8>(t)
        };
        masks.push_back(m);
    }
}

void cpu_MaskManager::create_triplets(void) {
    vector<bool> v {0, 1};
    for(auto&& t: iter::product(v, v, v)) {
        triplet_t m = {
            get<0>(t), get<1>(t), get<2>(t)
        };
        triplets.push_back(m);
    }
}

void cpu_MaskManager::create_maps(void) {
    for(unsigned int m=0; m < masks.size(); m++) {
        triplet_t triplet = triplets[m % triplets.size()];
        mask_t mask = masks[m];
        mapedMaks[triplet].push_back(mask);
        mapedTriplets[mask] = triplet;
    }
}
