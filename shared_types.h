#ifndef SHARED_TYPES_H
#define SHARED_TYPES_H

typedef unsigned int uint;

struct bgr_pixel {
    unsigned int R, G, B;
};

struct triplet_t {
    bool t0, t1, t2;

    bool operator==(const triplet_t& o) const {
        return (t0 == o.t0) && (t1 == o.t1) && (t2 == o.t2);
    }

    bool operator<(const triplet_t& o) const {
        int t = t0 << 2 | t1 << 1 | t2 << 0;
        int t2 = o.t0 << 2 | o.t1 << 1 | o.t2 << 0;
        return  t < t2;
    }
};

struct mask_t {
    bool m0, m1, m2, m3, m4, m5, m6, m7, m8;

    bool operator==(const mask_t& o) const {
        bool first_half = (m0 == o.m0 && m1 == o.m1 && m2 == o.m2 && m3 == o.m3 && m4 == o.m4);
        bool second_half = (m5 == o.m5 && m6 == o.m6 && m7 == o.m7 && m8 == o.m8);
        return first_half && second_half;
    }

    bool operator<(const mask_t& o) const {
        int f1 = m0 << 8 | m1 << 7 | m2 << 6 | m3 << 5 | m4 << 4 | m5 << 3 | m6 << 2 | m7 << 1 | m8 << 0;
        int f2 = o.m0 << 8 | o.m1 << 7 | o.m2 << 6 | o.m3 << 5 | o.m4 << 4 | o.m5 << 3;
        int f22 = f2 | o.m6 << 2 | o.m7 << 1 | o.m8 << 0;
        return f1 < f22;
    }
};

#endif // SHARED_TYPES_H
