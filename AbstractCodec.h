#ifndef ABSTRACTCODEC_H
#define ABSTRACTCODEC_H

#include <opencv2/opencv.hpp>

#include "constants.h"
#include "shared_types.h"
#include "cpu_MaskManager.h"
#include "Halftoning.h"

class AbstractCodec
{
    public:
        AbstractCodec();
        virtual ~AbstractCodec();
    protected:
        cpu_MaskManager mm;
        vector<mask_t> maskedHalftoneBlocks;

        mask_t getBlockAsMask(cv::Mat block);
        cv::Mat getMaskAsBlock(mask_t);
        void computeHalftoneMasks(cv::Mat);
    private:
};

#endif // ABSTRACTCODEC_H
