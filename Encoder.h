#ifndef ENCODER_H
#define ENCODER_H

#include <opencv2/opencv.hpp>

#include "constants.h"
#include "shared_types.h"
#include "Halftoning.h"
#include "AbstractCodec.h"

class Encoder : AbstractCodec
{
    public:
        Encoder(cv::Mat, cv::Mat, int, ENCODER_SOLVER es = CPU);
        virtual ~Encoder();
        cv::Mat getMasked(void);
        cv::Mat getHostHalftone(void);
        void setEncoderSolver(ENCODER_SOLVER);
        void setThreads(int);
    protected:
        cv::Mat hR, hG, hB;
        cv::Mat R, G, B;
        cv::Mat hostHalftone;
        cv::Mat codedHalftone;
        ENCODER_SOLVER encoderSolver;
        vector<triplet_t> triplets;
        vector<mask_t> embeddedMasks;
        int threads = 1;
    private:
        void setEmbeddedMasks(vector<mask_t>);
        void setRGB(cv::Mat);
        void computeHostHalftone(cv::Mat);
        void computeMarkHalftones(void);
        void computeTriplets(void);
        void embedMark(void);
        void convertEmbeddedMasksToHalftone(void);
};

#endif // ENCODER_H
