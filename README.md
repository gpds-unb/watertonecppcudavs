Contents
========

1.  [Description](#description)
2.  [License](#license)
3.  [Requirements](#requirements)
4.  [Installation](#installation)
5.  [Contact](#contact)
6.  [Contributing](#contributing)

Description
===========

This repository implements the method described in [1] using CUDA to solve the minimization steps.


License
=======

Watertone is released under [GNU GPL version
2.](http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt)


Requirements
============

WatertoneCPP was tested with VisualStudio 2013 in Windows 7 with CUDA 8.

Installation
===========

1. Clone the last version
> `#hg clone https://kuraiev@bitbucket.org/kuraiev/waterconecppcudavs`
2. Compile code using VisualStudio


Contact
=======

Please send all comments, questions, reports and suggestions (especially if you would like to contribute) to **sawp@sawp.com.br**

Contributing
============

If you would like to contribute with new algorithms, increment the code performance, documentation or another kind of modifications, please contact me.

References
==========

[1] FREITAS, Pedro Garcia; FARIAS, Mylene CQ; DE ARAÚJO, Aletéia PF. **Embedding Color Watermarks into Halftoning Images using Minimum-Distance Binary Patterns.** In: Graphics, Patterns and Images (SIBGRAPI), 2015 28th SIBGRAPI Conference on. IEEE, 2015. p. 56-63.

[2]  de Queiroz, Ricardo L., and Karen M. Braun. **Color to gray and back: color embedding into textured gray images.** Image Processing, IEEE transactions on 15.6 (2006): 1464-1470

[3] Ko, Kyung-Woo, et al. **Color embedding and recovery based on wavelet packet transform.** Journal of Imaging Science and Technology 52.1 (2008): 10501-1.
