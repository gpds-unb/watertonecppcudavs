#ifndef OMP_SOLVER_H
#define OMP_SOLVER_H

#include "cpu_MaskManager.h"
#include "binary_distances.cuh"
#include <omp.h>

namespace OMP_SOLVER {
    uint getMinimumIndex(std::vector<float> distances);
    vector<float> computeDistances(mask_t mask, vector<mask_t> possibleMasks);
    mask_t getBestMatch(triplet_t triplet, mask_t mask);
    std::vector<mask_t> computeBestMatches(vector<triplet_t> triplets, vector<mask_t> masks, int threads);
};

#endif