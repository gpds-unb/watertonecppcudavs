#include "FBIH.h"

#include <iostream>
#include <cstdlib>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

const float DEBLUR_FACTOR = 2.0;
const float NORM = 10e8;
const float THRESHOLD_DIFF = 0;
const float ERRTO = 0.25;
const int THRESHOLD_BLOCK_COUNT = 15;
const int BLOCK_SIZE = 5;

cv::Mat FBIH::inverseHalftoning(cv::Mat im) {
    cv::Mat floatMat;
    std::vector<float> floatImg = unpackCVMat(im);
    im.convertTo(floatMat, CV_32FC1);
    cv::Mat gaussianLvlOne = GaussianFilter1(floatMat);
    cv::Mat median = medianFilter3by3(gaussianLvlOne);
    cv::Mat gaussianLvlTwo = GaussianFilter2(median);
    cv::Mat gaussianLvlThree = GaussianFilter3(gaussianLvlTwo);
    cv::Mat quantiz = thresholdDiff(gaussianLvlTwo, gaussianLvlThree);
    cv::Mat normalized = normalise(quantiz, median);
    cv::Mat returnParam = rescale(normalized);

    returnParam.convertTo(returnParam, CV_8UC1);
    return returnParam;
}

std::vector<float> FBIH::unpackCVMat(cv::Mat img) {
    std::vector<float> matrix(img.rows * img.cols);
    for (int i = 0; i < img.rows; i++) {
        for (int j = 0; j < img.cols; j++) {
            int index = i * img.rows + j;
            matrix[index] = img.at<uchar>(i, j);
        }
    }
    return matrix;
}

cv::Mat FBIH::rescale(cv::Mat fullrange) {
    cv::Mat scaled = fullrange;
    double minv, maxv;
    cv::minMaxLoc(scaled, &minv, &maxv);
    scaled = 255.0 * (scaled - minv) / (maxv - minv);
    return scaled;
}

cv::Mat FBIH::medianFilter3by3(cv::Mat img) {
    uint rows = img.rows;
    uint cols = img.cols;
    cv::Mat output = cv::Mat::zeros(rows, cols, CV_32FC1);
    std::vector<float> block(9, 0.0);
    
    for(uint row = 0; row < rows - 1; row++) {
        for(uint col = 0; col < cols - 1; col++) {
            uint lastRow = labs(row - 1);
            uint lastCol = labs(col - 1);
            uint nextRow = labs(row + 1);
            uint nextCol = labs(col + 1);
            block[0] = (float) img.at<float>(lastRow, lastCol);
            block[1] = (float) img.at<float>(lastRow, col);
            if(nextCol >= cols) {
                block[2] = (float) img.at<float>(lastRow, 2 * cols - nextCol - 2);
                block[5] = (float) img.at<float>(row, 2 * cols - nextCol - 2);
            } else {
                block[2] = (float) img.at<float>(lastRow, nextCol);
                block[5] = (float) img.at<float>(row, nextCol);
            }
            block[3] = (float) img.at<float>(row, lastCol);
            block[4] = (float) img.at<float>(row, col);
            if (nextRow >= rows) {
                block[6] = (float) img.at<float>(2 * rows - nextRow - 2, lastCol);
                block[7] = (float) img.at<float>(2 * rows - nextRow - 2, col);
            } else {
                block[6] = (float) img.at<float>(nextRow, nextCol);
                block[7] = (float) img.at<float>(nextRow, col);
            }
            if (nextRow >= rows && nextCol >= cols)
                block[8] = (float) img.at<float>(2 * rows - nextRow - 2, 2 * cols - nextCol - 2);
            if (nextRow >= rows && nextCol < cols)
                block[8] = (float) img.at<float>(2 * rows - nextRow - 2, nextCol);
            if (nextRow < rows && nextCol >= cols)
                block[8] = (float) img.at<float>(nextRow, 2 * cols - nextCol - 2);
            if (nextRow < rows && nextCol < cols)
                block[8] = (float) img.at<float>(nextRow, nextCol);
            output.at<float>(row, col) = getPixel(5, 9, block);
        }
    }
    return output;
}

template<class T> inline void FBIH::SWAP(T& x, T& y) {
    return std::swap(x, y);
}

float FBIH::getPixel(uint upos, uint blocksize, std::vector<float> blk) {
    int pos = upos;
    std::vector<float> block(blk);
    int l = 0;
    int ir = blocksize - 1;
    while (ir > l + 1) {
        int i = l + 1;
        int j = ir;
        int mid = (int) ((l + ir) / 2);
        SWAP(block[l+1], block[mid]);
        if (block[l] > block[ir])
            SWAP(block[ir], block[l]);
        if (block[l+1] > block[ir])
            SWAP(block[ir], block[l+1]);
        if (block[l] > block[l+1])
            SWAP(block[l], block[l+1]);
        float selected = block[l+1];
        do {
            do {
                i = i + 1;
            } while (block[i] < selected);
            do {
                j = j - 1;
            } while (block[j] > selected);
            SWAP(block[j], block[i]);
        } while (j > i);
        block[l+1] = block[j];
        block[j] = selected;
        if (j >= pos)
            ir = j - 1;
        if (j <= pos)
            l = i;
    }
    if (ir == l + 1  && block[ir] < block[l])
        SWAP(block[ir], block[l]);
    return block[pos];
}

cv::Mat FBIH::GaussianFilter1(cv::Mat s) {
    std::vector<int> filter = {11, 135, 808, 2359, 3372, 2359, 808, 135, 11};
    return separable9x9FIRBinaryImage(s, filter);
}

cv::Mat FBIH::separable9x9FIRBinaryImage(cv::Mat source, std::vector<int> filter) {
    cv::Mat ws = rowConvolutions9by9(source, filter);
    return columnConvolutions9by9(ws, filter);
}

cv::Mat FBIH::rowConvolutions9by9(cv::Mat src, std::vector<int> filter) {
    uint rows = src.rows;
    uint cols = src.cols;
    cv::Mat ws = cv::Mat::zeros(rows, cols, CV_32FC1);

    for (uint i = 0; i < rows; i++) {
         const float* t = (float *) src.ptr<float>(i);
         std::vector<float> sourceRow(t, t + cols);
        for (uint j = 0; j < cols; j++) {
            std::vector<int> p(9);
            p[0] = j - 4;
            p[1] = j - 3;
            p[2] = j - 2;
            p[3] = j - 1;
            p[4] = j;
            p[5] = j + 1;
            p[6] = j + 2;
            p[7] = j + 3;
            p[8] = j + 4;
            p[0] = abs(p[0]);
            p[1] = abs(p[1]);
            p[2] = abs(p[2]);
            p[3] = abs(p[3]);
            p[5] = reflect(p[5], cols);
            p[6] = reflect(p[6], cols);
            p[7] = reflect(p[7], cols);
            p[8] = reflect(p[8], cols);
            float sums = 0.0;
            for (int k = 0; k < 9; k++)
                if (sourceRow[p[k]] != 0)
                    sums = sums + (float) filter[k];
            ws.at<float>(i, j) = 255 * sums;
        }
    }
    return ws;
}

cv::Mat FBIH::columnConvolutions9by9(cv::Mat src, std::vector<int> filter) {
    uint rows = src.rows;
    uint cols = src.cols;
    cv::Mat dest = cv::Mat::zeros(rows, cols, CV_32FC1);

    for (uint j = 0; j < cols; j++) {
        for (uint i = 0; i < rows; i++) {
            std::vector<int> p(9);
            p[0] = i - 4;
            p[1] = i - 3;
            p[2] = i - 2;
            p[3] = i - 1;
            p[4] = i;
            p[5] = i + 1;
            p[6] = i + 2;
            p[7] = i + 3;
            p[8] = i + 4;
            p[0] = abs(p[0]);
            p[1] = abs(p[1]);
            p[2] = abs(p[2]);
            p[3] = abs(p[3]);
            p[5] = reflect(p[5], rows);
            p[6] = reflect(p[6], rows);
            p[7] = reflect(p[7], rows);
            p[8] = reflect(p[8], rows);
            float sums = filter[4] * src.at<float>(i, j);
            sums += filter[0] * (src.at<float>(p[0], j) + src.at<float>(p[8], j));
            sums += filter[1] * (src.at<float>(p[1], j) + src.at<float>(p[7], j));
            sums += filter[2] * (src.at<float>(p[2], j) + src.at<float>(p[6], j));
            sums += filter[3] * (src.at<float>(p[3], j) + src.at<float>(p[5], j));
            dest.at<float>(i, j) = sums / NORM + ERRTO;
        }
    }
    return dest;
}

cv::Mat FBIH::GaussianFilter2(cv::Mat s) {
    std::vector<int> filter = {44, 540, 2420, 3991, 2420, 540, 44};
    return separable7x7FIRGreyImage(s, filter);
}

cv::Mat FBIH::GaussianFilter3(cv::Mat s) {
    std::vector<int> filter = {1, 103, 2075, 5641, 2075, 103, 1};
    return separable7x7FIRGreyImage(s, filter);
}

cv::Mat FBIH::separable7x7FIRGreyImage(cv::Mat source, std::vector<int> filter) {
    cv::Mat ws = rowConvolutions7by7(source, filter);
    return columnConvolutions7by7(ws, filter);
}

cv::Mat FBIH::rowConvolutions7by7(cv::Mat src, std::vector<int> filter) {
    uint rows = src.rows;
    uint cols = src.cols;
    cv::Mat ws = cv::Mat::zeros(rows, cols, CV_32FC1);

    for (uint i = 0; i < rows; i++) {
         const float* t = (float *) src.ptr<float>(i);
         std::vector<float> sourceRow(t, t + cols);
        for (uint j = 0; j < cols; j++) {
            int p2 = labs(j - 3);
            int p3 = labs(j - 2);
            int p4 = labs(j - 1);
            int p6 = j + 1;
            int p7 = j + 2;
            int p8 = j + 3;
            p6 = reflect(p6, cols);
            p7 = reflect(p7, cols);
            p8 = reflect(p8, cols);
            ws.at<float>(i, j) = (filter[3] * sourceRow[j]);
            ws.at<float>(i, j) += filter[0] * (sourceRow[p2] + sourceRow[p8]);
            ws.at<float>(i, j) += filter[1] * (sourceRow[p3] + sourceRow[p7]);
            ws.at<float>(i, j) += filter[2] * (sourceRow[p4] + sourceRow[p6]);
        }
    }
    return ws;
}


cv::Mat FBIH::columnConvolutions7by7(cv::Mat src, std::vector<int> filter) {
    uint rows = src.rows;
    uint cols = src.cols;
    cv::Mat dest = cv::Mat::zeros(rows, cols, CV_32FC1);

    for (uint j = 0; j < cols; j++) {
        for (uint i = 0; i < rows; i++) {
            int p2 = labs(i - 3);
            int p3 = labs(i - 2);
            int p4 = labs(i - 1);
            int p6 = i + 1;
            int p7 = i + 2;
            int p8 = i + 3;
            p6 = reflect(p6, rows);
            p7 = reflect(p7, rows);
            p8 = reflect(p8, rows);
            float sums = filter[3] * src.at<float>(i,j);
            sums += filter[0] * (src.at<float>(p2,j) + src.at<float>(p8,j));
            sums += filter[1] * (src.at<float>(p3,j) + src.at<float>(p7,j));
            sums += filter[2] * (src.at<float>(p4,j) + src.at<float>(p6,j));
            dest.at<float>(i, j) = sums / NORM + ERRTO;
        }
    }
    return dest;
}

int FBIH::reflect(int x, int y) {
    return (x >= y) ? 2 * y - x - 2 : x;
}

cv::Mat FBIH::thresholdDiff(cv::Mat xi, cv::Mat zi) {
    uint rows = xi.rows;
    uint cols = xi.cols;
    cv::Mat hie = cv::Mat::zeros(rows, cols, CV_32FC1);
    cv::Mat x = xi.clone();
    cv::Mat z = zi.clone();
    cv::Mat edgeMap = binaryMedialFilter5by5(z);

    for (uint i = 0; i < rows; i++) {
        for (uint j = 0; j < cols; j++) {
            float pixel = x.at<float>(i,j) - z.at<float>(i,j);
            if (pixel <= THRESHOLD_DIFF && pixel >= -THRESHOLD_DIFF)
                z.at<float>(i,j) = 0;
            else
                z.at<float>(i,j) = 1.0;
            hie.at<float>(i, j) = pixel;
        }
    }
    for (uint i = 0; i < rows; i++) {
        for (uint j = 0; j < cols; j++) {
            hie.at<float>(i, j) = hie.at<float>(i, j) * z.at<float>(i,j) * edgeMap.at<float>(i,j);
        }
    }
    return hie;
}

cv::Mat FBIH::binaryMedialFilter5by5(cv::Mat img) {
    uint rows = img.rows;
    uint cols = img.cols;
    cv::Mat filtered = cv::Mat::zeros(rows, cols, CV_32FC1);

    for (uint row = 0; row < rows; row++) {
        for (uint col = 0; col < cols; col++) {
            int ones = bitBlockCounter(row, col, img);
            filtered.at<float>(row, col) = (ones > THRESHOLD_BLOCK_COUNT) ? 1.0f : 0.0f;
        }
    }
    return filtered;
}

int FBIH::bitBlockCounter(int row, int col, cv::Mat img) {
    uint countOnes = 0;
    for (uint blockX = 0; blockX <= BLOCK_SIZE; blockX++) {
        int r = row - blockX;
        for (uint blockY = 0; blockY <= BLOCK_SIZE; blockY++) {
            int c = col - blockY;
            if (r >= 0 && c >= 0) {
                if (img.at<float>(r, c) > 0)
                    countOnes++;
                if (countOnes > THRESHOLD_BLOCK_COUNT)
                    break;
            }
        }
        if (countOnes > THRESHOLD_BLOCK_COUNT)
            break;
    }
    return countOnes;
}

cv::Mat FBIH::normalise(cv::Mat hie, cv::Mat y1) {
    uint rows = hie.rows;
    uint cols = hie.cols;
    cv::Mat outputImage = cv::Mat::zeros(hie.rows, hie.cols, CV_32FC1);

    for (uint i = 0; i < rows; i++) {
        for (uint j = 0; j < cols; j++) {
            float h = (float) hie.at<float>(i,j);
            float y = (float) y1.at<float>(i,j);
            float outputValue = ERRTO + DEBLUR_FACTOR * (h + y);
            outputImage.at<float>(i,j) = outputValue;
        }
    }
    return outputImage;
}

FBIH::FBIH() {
    //ctor
}

FBIH::~FBIH() {
    //dtor
}
