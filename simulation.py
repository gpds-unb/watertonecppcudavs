import subprocess
import shlex
import os

images = ['Airplane', 'Baboon', 'Girl', 'house', 'Lena', 'Peppers', 'Sailboat', 'Splash']
configurations = ['Release', 'Debug']
devices = ['GPU', 'CPU']

for image in images:
    print("---------------{image}---------------".format(image=image))
    for device in devices:
        for config in configurations:
            command = "{basedir}/bin/win64/{config}/WatertoneCUDA.exe -e {device} -i ./resources/images/original/{image}.png -c ./resources/images/coded/{image}.png -o ./resources/images/restored/{image}.png".format(basedir=os.getcwd(), config=config, device=device, image=image)
            print("Image: ", image, " config: ", config, " device: ", device)
            subprocess.call(command, shell=True)
            print("\n")
    print("-------------------------------------")