#ifndef FBIH_H
#define FBIH_H

#include <opencv2/opencv.hpp>
#include "shared_types.h"

class FBIH
{
    public:
        static cv::Mat inverseHalftoning(cv::Mat);
    protected:
        static cv::Mat medianFilter3by3(cv::Mat);
        template<class T> static void SWAP(T&, T&);
        static float getPixel(uint, uint, std::vector<float>);
        static cv::Mat GaussianFilter1(cv::Mat);
        static cv::Mat separable9x9FIRBinaryImage(cv::Mat, std::vector<int>);
        static cv::Mat rowConvolutions9by9(cv::Mat, std::vector<int>);
        static cv::Mat columnConvolutions9by9(cv::Mat, std::vector<int>);
        static cv::Mat GaussianFilter2(cv::Mat);
        static cv::Mat GaussianFilter3(cv::Mat);
        static cv::Mat separable7x7FIRGreyImage(cv::Mat, std::vector<int>);
        static cv::Mat rowConvolutions7by7(cv::Mat, std::vector<int>);
        static cv::Mat columnConvolutions7by7(cv::Mat, std::vector<int>);
        static int reflect(int, int);
        static cv::Mat thresholdDiff(cv::Mat, cv::Mat);
        static cv::Mat binaryMedialFilter5by5(cv::Mat);
        static int bitBlockCounter(int, int, cv::Mat);
        static cv::Mat normalise(cv::Mat, cv::Mat);
        static cv::Mat rescale(cv::Mat);
        static std::vector<float> unpackCVMat(cv::Mat);
    private:
        FBIH();
        virtual ~FBIH();
};

#endif // FBIH_H
