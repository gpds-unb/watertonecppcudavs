for /l %%y in (1, 1, 20) do (
    for /l %%x in (1, 1, 10) do (
       start /b /wait /affinity 1 .\bin\win64\Debug\WatertoneCUDA.exe -e CPU -r %%y
       start /b /wait /affinity 1 .\bin\win64\Release\WatertoneCUDA.exe -e GPU -r %%y
    )
)