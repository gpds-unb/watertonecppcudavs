#ifndef BINARY_DISTANCES_H
#define BINARY_DISTANCES_H

#include <thrust/execution_policy.h>
#include "shared_types.h"


namespace BinaryDistances {
    __host__ __device__ float getDiceDissimilarity(mask_t u, mask_t v);
};

#endif