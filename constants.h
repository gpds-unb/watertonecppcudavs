#ifndef CONSTANTS_H
#define CONSTANTS_H
const int BLOCK_SIZE = 3;

enum ENCODER_SOLVER {
    GPU, CPU, OMP
};

#endif // SHARED_TYPES_H
