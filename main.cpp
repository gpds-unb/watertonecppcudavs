#include <iostream>
#include <vector>
#include <tuple>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <omp.h>
#include <chrono>

#include "ImageManager.h"
#include "Halftoning.h"
#include "Encoder.h"
#include "Decoder.h"
#include "FBIH.h"

using namespace std::chrono;

ENCODER_SOLVER es = CPU;
std::string in;
std::string half;
std::string out;
int concatenateImages = 1;
int threads = 1;

void PrintFullPath(char * partialPath)
{
    char full[_MAX_PATH];
    if (_fullpath(full, partialPath, _MAX_PATH) != NULL)
        printf("Full path is: %s\n", full);
    else
        printf("Invalid path\n");
}

void setEncoderSolver(std::string encs) {
    if (encs == "GPU" || encs == "gpu")
        es = GPU;
    else if (encs == "OMP" || encs == "omp")
        es = OMP;
    else
        es = CPU;
}

void printHelp(char* progName) {
    std::cout << progName << " [options] " << endl <<
        "Options:" << endl <<
        "-h | --help                            Print this help" << endl <<
        "-e | --encode-solver <*CPU|GPU|OMP>    Chose the encoder type" << endl <<
        "-i | --in <input image>                Input image" << endl <<
        "-c | --code <code halftone>            Halftone image" << endl <<
        "-o | --out <output image>              Decoded image" << endl <<
        "-t | --threads <number>                OMP threads" << endl <<
        "-r | --repeat <number>                 Number of images" << endl <<
        std::endl;
    exit(0);
}

void parseArguments(int argc, char* argv[]) {
    for (int i = 1; i < argc; i++) {
        std::string argument = argv[i];
        if (argument == "-e" || argument == "--encode-solver") {
            string encoder = argv[i + 1];
            setEncoderSolver(encoder);
        }
        else if (argument == "-h" || argument == "--help") {
            printHelp(argv[0]);
        }
        else if (argument == "-i" || argument == "--in") {
            in = argv[i + 1];
        }
        else if (argument == "-o" || argument == "--out") {
            out = argv[i + 1];
        }
        else if (argument == "-c" || argument == "--code") {
            half = argv[i + 1];
        }
        else if (argument == "-t" || argument == "--threads") {
            string s = argv[i + 1];
            threads = std::stoi(s);
        }
        else if (argument == "-r" || argument == "--repeat") {
            string s = argv[i + 1];
            concatenateImages = std::stoi(s);
        }
    }
}


cv::Mat getBigImage(int size)  {
    //std::vector<string> filenames = { "Beach.jpg", "Candies.jpg", "chicken.jpg", "City.jpg", "Cupdake.jpg", "Earth.jpg", "Festival.jpg", "Jet.jpg" };
    std::vector<string> filenames = { "Airplane.png", "Baboon.png", "Girl.png", "house.png", "Lena.png", "Peppers.png", "Sailboat.png", "Splash.png" };

    if (filenames.size() < size) {
        for (int i = 0; i < size; i += size) {
            auto old_count = filenames.size();
            filenames.reserve(size + old_count);
            std::copy_n(filenames.begin(), old_count, std::back_inserter(filenames));
        }
    }

    std::vector<cv::Mat> images(filenames.size());
    std::transform(filenames.begin(), filenames.end(), images.begin(),
        [](string s)->cv::Mat {
        return cv::imread("resources/images/original/" + s);
    });
    cv::Mat ret = images[0];
    for (int i = 1; i < size; i++)
        cv::hconcat(ret, images[i], ret);
    return ret;
}


int main(int argc, char* argv[])
{
    parseArguments(argc, argv);
    //cv::Mat image = cv::imread(in);

    //int size = 16;
    int size = concatenateImages;
    cv::Mat image = getBigImage(size);
    //cv::Mat image = cv::imread("resources/images/original/Lena.png");
    //cv::resize(image, image, cv::Size(image.cols / size, image.rows / size));
    long pixels = image.rows * image.cols;
    half = "resources/images/coded/bigimage.png";
    out = "resources/images/restored/bigimage.png";
    //es = GPU;

    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    Encoder enc(image, image, threads, es);

    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    auto duration_encoder = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();

    cv::imwrite(half, enc.getMasked());

    /*high_resolution_clock::time_point t3 = high_resolution_clock::now();
    Decoder dec(enc.getMasked());
    high_resolution_clock::time_point t4 = high_resolution_clock::now();
    auto duration_decoder = std::chrono::duration_cast<std::chrono::nanoseconds>(t4 - t3).count();
    cv::imwrite(out, dec.getDecoded());*/

    //std::cout << "Encoding: " << duration_encoder / 1e9 << " Decoding: " << duration_decoder / 1e9 << std::endl;
    std::cout << "Encoding: " << duration_encoder / 1e9 << std::endl;
    std::cout << "Size: " << size << " Pixels: " << pixels << std::endl;

    return 0;
}
