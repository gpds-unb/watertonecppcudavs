#ifndef PRODUCT_H
#define PRODUCT_H

#include<tuple>
#include<vector>

namespace iter {

    template<typename F>
    inline void product_imp(F f) {
        f();
    }

    template<typename F, typename H, typename... Ts>
    inline void product_imp(F f, std::vector<H> const& h,
        std::vector<Ts> const&... t) {
        for (H const& he : h)
            product_imp([&]
            (Ts const&... ts){
            f(he, ts...);
        }, t...);
    }

    template<typename... Ts>
    std::vector<std::tuple<Ts...>> product(std::vector<Ts> const&... in) {
        std::vector<std::tuple<Ts...>> res;
        product_imp([&](Ts const&... ts){
            res.emplace_back(ts...);
        }, in...);
        return res;
    }

};

#endif