#include "Halftoning.h"

using namespace cv;

namespace Halftoning {
    Mat floydSteinberg(Mat src) {
        Mat gray  = src.clone();
        if(gray.type() != CV_8UC1) {
            cvtColor(gray, gray, CV_BGR2GRAY);
        }

        int rows = gray.rows;
        int cols = gray.cols;
        float* half = new float[rows * cols];
        for(int row=0; row < rows; row++) {
            for(int col=0; col < cols; col++) {
                int index = row * cols + col;
                half[index] = (float) gray.at<uchar>(row, col);
            }
        }

        for(int row=0; row < rows; row++) {
            for(int col=0; col < cols; col++) {
                int index = row * cols + col;
                float error = (half[index] >= 128) ? (half[index] - 255.0f) : half[index];
                half[index] = (half[index] >= 128) ? 255.0f : 0.0f;

                if (col + 1 < cols) {
                    int index1 = row * cols + (col + 1);
                    half[index1] += error * 0.4375f;
                }

                if ((row + 1 < rows) && (col + 1 < cols)) {
                    int index2 = (row + 1) * cols + (col + 1);
                    half[index2] += error * 0.0625f;
                }

                if (row + 1 < rows) {
                    int index3 = (row + 1) * cols + col;
                    half[index3] += error * 0.3125f;
                }

                if((row + 1 < rows) && (col - 1 >= 0)) {
                    int index4 = (row + 1) * cols + (col - 1);
                    half[index4] += error * 0.1875f;
                }
            }
        }

        Mat dst = gray.clone();
        dst.convertTo(dst, CV_8UC1);


        for(int row=0; row < rows; row++) {
            for(int col=0; col < cols; col++) {
                int index = row * cols + col;
                dst.at<uchar>(row, col) = (uchar) half[index];
            }
        }
        return dst;
    }
}
