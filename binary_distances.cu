#include "binary_distances.cuh"
#include <thrust/execution_policy.h>
#include "shared_types.h"


namespace BinaryDistances {
    __host__ __device__ float getDiceDissimilarity(mask_t u, mask_t v) {
        int nft = (!u.m0 & v.m0);
        nft += (!u.m1 & v.m1);
        nft += (!u.m2 & v.m2);
        nft += (!u.m3 & v.m3);
        nft += (!u.m4 & v.m4);
        nft += (!u.m5 & v.m5);
        nft += (!u.m5 & v.m6);
        nft += (!u.m7 & v.m7);
        nft += (!u.m8 & v.m8);
        int ntf = (u.m0 & !v.m0);
        ntf += (u.m1 & !v.m1);
        ntf += (u.m2 & !v.m2);
        ntf += (u.m3 & !v.m3);
        ntf += (u.m4 & !v.m4);
        ntf += (u.m5 & !v.m5);
        ntf += (u.m5 & !v.m6);
        ntf += (u.m7 & !v.m7);
        ntf += (u.m8 & !v.m8);
        int ntt = (u.m0 & v.m0);
        ntt += (u.m1 & v.m1);
        ntt += (u.m2 & v.m2);
        ntt += (u.m3 & v.m3);
        ntt += (u.m4 & v.m4);
        ntt += (u.m5 & v.m5);
        ntt += (u.m5 & v.m6);
        ntt += (u.m7 & v.m7);
        ntt += (u.m8 & v.m8);
        float dissimilarity = (float)(ntf + nft) / (2.0f * ntt + ntf + nft);
        return dissimilarity;
    }
};