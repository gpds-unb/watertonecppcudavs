#include "GPU_SOLVER.cuh"
#include "binary_distances.cuh"
#include "gpu_maskmanager.cuh"
#include "shared_types.h"

#include <thrust/copy.h>

namespace GPU_SOLVER {
    const unsigned int N = 64;

    __host__ __device__ unsigned int getMinimumIndex(float* distances) {
        unsigned int minIndex = 0;
        float minimal = NPP_MAXABS_32F;
        for (unsigned int i = 0; i < N; i++) {
            float d = distances[i];
            if (d < minimal) {
                minimal = d;
                minIndex = i;
            }
        }
        return minIndex;
    }

    __host__ __device__ void computeDistances(mask_t mask, mask_t* possibleMasks, float* distances) {
        for (unsigned int v = 0; v < N; v++) {
            mask_t possibleMask = possibleMasks[v];
            distances[v] = BinaryDistances::getDiceDissimilarity(mask, possibleMask);
            //if (threadIdx.x == 0)
            //printf("Iterator: %d -> distance: %f\n", v, distances[v]);
        }
    }

    __host__ __device__ mask_t getBestMatch(triplet_t triplet, mask_t mask, mask_t* masks) {
        // unsigned int idx = gpu_maskmanager::tripletAsIndex(triplet);
        mask_t* possibleMasks = masks;
        float distances[N];
        computeDistances(mask, possibleMasks, distances);
        unsigned int minDistancePosition = getMinimumIndex(distances);
        mask_t matchedMask = possibleMasks[minDistancePosition];
        //printf("minDistancePosition: %d\n", minDistancePosition);
        return matchedMask;
        //return mask;
    }

    //struct best_match_functor : public thrust::binary_function < triplet_t, mask_t, mask_t > {
    //    thrust::device_vector<mask_t>* masks;

    //    best_match_functor(thrust::device_vector<mask_t>* m) : masks(m) {};

    //    __device__ mask_t operator()(const triplet_t& x, const mask_t& y) {
    //        return getBestMatch(x, y, masks);
    //    }
    //};

    struct best_match_functor : public thrust::binary_function < triplet_t, mask_t, mask_t > {
        thrust::device_vector<mask_t> dvm0, dvm1, dvm2, dvm3, dvm4, dvm5, dvm6, dvm7;
        mask_t *masks0, *masks1, *masks2, *masks3, *masks4, *masks5, *masks6, *masks7;

        best_match_functor(thrust::device_vector<mask_t> const& m0,
            thrust::device_vector<mask_t> const& m1,
            thrust::device_vector<mask_t> const& m2,
            thrust::device_vector<mask_t> const& m3,
            thrust::device_vector<mask_t> const& m4,
            thrust::device_vector<mask_t> const& m5,
            thrust::device_vector<mask_t> const& m6,
            thrust::device_vector<mask_t> const& m7)
            : dvm0(m0)
            , dvm1(m1)
            , dvm2(m2)
            , dvm3(m3)
            , dvm4(m4)
            , dvm5(m5)
            , dvm6(m6)
            , dvm7(m7)
            , masks0(thrust::raw_pointer_cast(dvm0.data()))
            , masks1(thrust::raw_pointer_cast(dvm1.data()))
            , masks2(thrust::raw_pointer_cast(dvm2.data()))
            , masks3(thrust::raw_pointer_cast(dvm3.data()))
            , masks4(thrust::raw_pointer_cast(dvm4.data()))
            , masks5(thrust::raw_pointer_cast(dvm5.data()))
            , masks6(thrust::raw_pointer_cast(dvm6.data()))
            , masks7(thrust::raw_pointer_cast(dvm7.data()))
        {};

       __host__ __device__ mask_t operator()(const triplet_t& x, const mask_t& y) {
            mask_t* ptrM[8] = { masks0, masks1, masks2, masks3, masks4, masks5, masks6, masks7 };
            unsigned int idx = gpu_maskmanager::tripletAsIndex(x);

            return getBestMatch(x, y, ptrM[idx]);
        }
    };

    std::vector<mask_t> computeBestMatches(vector<triplet_t> triplets, vector<mask_t> masks) {
        gpu_maskmanager::init();

        thrust::device_vector<mask_t> mm0 = gpu_maskmanager::mapedMaks0;
        thrust::device_vector<mask_t> mm1 = gpu_maskmanager::mapedMaks1;
        thrust::device_vector<mask_t> mm2 = gpu_maskmanager::mapedMaks2;
        thrust::device_vector<mask_t> mm3 = gpu_maskmanager::mapedMaks3;
        thrust::device_vector<mask_t> mm4 = gpu_maskmanager::mapedMaks4;
        thrust::device_vector<mask_t> mm5 = gpu_maskmanager::mapedMaks5;
        thrust::device_vector<mask_t> mm6 = gpu_maskmanager::mapedMaks6;
        thrust::device_vector<mask_t> mm7 = gpu_maskmanager::mapedMaks7;

        thrust::device_vector<triplet_t> triplet_device(triplets.size());
        thrust::device_vector<mask_t> mask_device(masks.size());
        thrust::device_vector<mask_t> chosed_masks(masks.size());

        thrust::copy(triplets.begin(), triplets.end(), triplet_device.begin());
        thrust::copy(masks.begin(), masks.end(), mask_device.begin());

        thrust::transform(thrust::device, triplet_device.begin(), triplet_device.end(), mask_device.begin(), chosed_masks.begin(),
            best_match_functor(mm0, mm1, mm2, mm3, mm4, mm5, mm6, mm7));

        std::vector<mask_t> stdChosenMask(chosed_masks.size());
        thrust::copy(chosed_masks.begin(), chosed_masks.end(), stdChosenMask.begin());
        return stdChosenMask;
    }
};
