#ifndef CPU_SOLVER_H
#define CPU_SOLVER_H

#include "cpu_MaskManager.h"
#include "binary_distances.cuh"

namespace CPU_SOLVER {
    uint getMinimumIndex(std::vector<float> distances) {
        uint minIndex = 0;
        float minimal = std::numeric_limits<float>::max();
        for (uint i = 0; i < distances.size(); i++) {
            float d = distances[i];
            if (d < minimal) {
                minimal = d;
                minIndex = i;
            }
        }
        return minIndex;
    }

    vector<float> computeDistances(mask_t mask, vector<mask_t> possibleMasks) {
        vector<float> distances(possibleMasks.size());
        for (uint v = 0; v < possibleMasks.size(); v++) {
            mask_t possibleMask = possibleMasks[v];
            distances[v] = BinaryDistances::getDiceDissimilarity(mask, possibleMask);
        }
        return distances;
    }

    mask_t getBestMatch(triplet_t triplet, mask_t mask) {
        static cpu_MaskManager mm;
        std::vector<mask_t> possibleMasks = mm.getMasks(triplet);
        std::vector<float> distances = computeDistances(mask, possibleMasks);
        uint minDistancePosition = getMinimumIndex(distances);
        mask_t matchedMask = possibleMasks[minDistancePosition];
        return matchedMask;
    }

    std::vector<mask_t> computeBestMatches(vector<triplet_t> triplets, vector<mask_t> masks) {
        std::vector<mask_t> stdChosenMask(masks.size());
        for (uint v = 0; v < triplets.size(); v++) {
            triplet_t t = triplets[v];
            mask_t b = masks[v];
            stdChosenMask[v] = getBestMatch(t, b);
        }
        return stdChosenMask;
    }
};

#endif